from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignUpForm(UserCreationForm):
    username= forms.CharField(max_length=12, help_text='Type your Register No if you are a student , College id if you are a staff')
    first_name = forms.CharField(max_length=100, help_text='')
    last_name = forms.CharField(max_length=100, help_text='')
    email = forms.EmailField(max_length=150, help_text='Enter a valid Email, we will send a activation link to this account.')



    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2','is_staff' )

