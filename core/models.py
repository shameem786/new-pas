from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from phone_field import PhoneField

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=150)
    signup_confirmation = models.BooleanField(default=False)
    admin_approve=models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

#profile object is created when a new User model object created
@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Staff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    college_id=models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.user.username

#Staff object is created when a new User model with is_staff is true created
@receiver(post_save, sender=User)
def update_staff_signal(sender, instance, created, **kwargs):
    if created:
        if instance.is_staff == True:
            Staff.objects.create(user=instance)
            instance.staff.save()


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    admission_no=models.CharField(max_length=20, blank=True)
    roll_no = models.CharField(max_length=20, blank=True)
    mobile_no=PhoneField(blank=True, help_text='Contact phone number')
    batch=models.IntegerField(default=0)

    def __str__(self):
        return self.user.username



class Project(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    topic=models.CharField(max_length=200, blank=True)
    domain=models.CharField(max_length=100, blank=True)
    abstract=models.FileField(upload_to='abstract/')
    basepaper=models.FileField(upload_to='basepaper/')
    report=models.FileField(upload_to='report/')
    reject_no=models.IntegerField(blank=True,default=0)
    type=models.CharField(max_length=20, blank=True)

    def delete(self, *args, **kwargs):
        # first, delete the file
        self.abstract.delete(save=False)
        self.basepaper.delete(save=False)
        self.report.delete(save=False)

        # now, delete the object
        super(Project, self).delete(*args, **kwargs)
    def __str__(self):
        return self.user.username




class Panel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    guide=models.IntegerField(blank=True,default=False)
    panel1=models.IntegerField(blank=True,default=False)
    panel2=models.IntegerField(blank=True,default=False)
    panel3=models.IntegerField(blank=True,default=False)
    guide_status=models.CharField(max_length=20, blank=True)
    panel1_status=models.CharField(max_length=20, blank=True)
    panel2_status=models.CharField(max_length=20, blank=True)
    panel3_status=models.CharField(max_length=20, blank=True)
    admin_status=models.CharField(max_length=20, blank=True)
    Notes=models.CharField(max_length=500, blank=True)


    def __str__(self):
        return self.user.username


class Date(models.Model):
    title=models.CharField(max_length=20, blank=True)
    type=models.CharField(max_length=20, blank=True)
    last_date=models.DateTimeField()
    batch=models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Document(models.Model):
    title=models.CharField(max_length=20, blank=True)
    doc=models.FileField(upload_to='document/')
    def delete(self, *args, **kwargs):
        # first, delete the file
        self.doc.delete(save=False)
        # now, delete the object
        super(Document, self).delete(*args, **kwargs)
    def __str__(self):
        return self.title



#Student object is created when a new User model with is_staff is false created
@receiver(post_save, sender=User)
def update_student_signal(sender, instance, created, **kwargs):
    if created:
        if instance.is_staff == False:
            Student.objects.create(user=instance)
            Panel.objects.create(user=instance)
            Project.objects.create(user=instance)
            instance.student.save()
            instance.panel.save()
            instance.project.save()







