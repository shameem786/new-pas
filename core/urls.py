from django.urls import path, include
from core.views import login_view, signup_view, activation_sent_view, activate, admin_view, logout_view, student_view,under_guidance_batch_username, \
    staff_view, report_date,abstract_date,abstract_date_set,report_date_set,sample_abstract,under_guidance_batch,under_guidance,\
    registration_approve, student_profile, add_staff, complete_profile, profile_edit, change_password, upload_abstract,approve_reject, \
    upload_report, edit_student,edit_staff,staff_edit,staff_edit_update,create_panel,create_panel_batch,deactivate_user,under_panel,under_panel_batch,\
    create_abstract, Pdf,activate_user,edit_panel,edit_panel_batch,edit_panel_student,edit_panel_student_confirm,change_admin,signup_confirm,signup_approve,\
    approve_batch,approve_batch_student,student_detail_show,student_edit,student_edit_update,Pdf1,download_page,student_list_admin,Pdf2,my_panel
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('',login_view,name='login_view'),
    path('logout',logout_view,name='logout_view'),
    path('signup/', signup_view, name="signup"),
    path('sent/', activation_sent_view, name="activation_sent"),
    path('activate/<slug:uidb64>/<slug:token>/', activate, name='activate'),
    path('', include('django.contrib.auth.urls')),#urls for forgot password
    path('admin_home/',admin_view,name='admin'),
    path('admin/download/student_list',student_list_admin,name='student_list_admin'),
    path('admin/download/student_list/<str:batch>',login_required(login_url='login_view')(Pdf2.as_view())),
    path('admin/approve_reject',approve_batch,name='approve_select_batch'),
    path('admin/approve_reject/<str:batch>',approve_batch_student),
    path('admin/approve_reject/<str:batch>/<str:username>',student_detail_show),
    path('admin/approve_reject/<str:batch>/<str:username>/approve_reject', approve_reject),
    path('admin/change_admin',change_admin,name='change_admin'),
    path('admin/signup_confirm',signup_confirm,name='signup_confirm'),
    path('admin/change_password',change_password,name='admin_change_password'),
    path('admin/edit_profile',profile_edit,name='admin_profile_edit'),
    path('admin_home/reg_approve/<str:username>',registration_approve),
    path('admin/signup_approve/<str:username>',signup_approve),
    path('admin/abstract_date',abstract_date,name='abstract_date'),
    path('admin/abstract_date/<str:batch>',abstract_date_set,name='abstract_date_set'),
    path('admin/report_date',report_date,name='report_date'),
    path('admin/report_date/<str:batch>',report_date_set,name='report_date_set'),
    path('admin/student/add',student_profile,name='admin_student'),
    path('admin/student/edit',edit_student,name='edit_student'),
    path('admin/student/edit/<str:username>',student_edit),
    path('admin/student/edit/edit/<str:username>',student_edit_update),
    path('admin/student/add_abstract',upload_abstract,name='admin_abstract'),
    path('admin/student/add_report',upload_report,name='admin_report'),
    path('admin/user/activate',activate_user,name='student_activate'),
    path('admin/user/deactivate',deactivate_user,name='student_deactivate'),
    path('admin/staff/add',add_staff,name='add_staff'),
    path('admin/staff/edit',edit_staff,name='edit_staff'),
    path('admin/staff/edit/<str:username>',staff_edit),
    path('admin/staff/edit/edit/<str:username>', staff_edit_update),
    path('admin/panel/create', create_panel,name='create_panel'),
    path('admin/panel/create/<str:batch>', create_panel_batch,name='create_panel_batch'),
    path('admin/panel/edit', edit_panel,name='edit_panel'),
    path('admin/panel/edit/<str:batch>', edit_panel_batch),
    path('admin/panel/edit/<str:batch>/<str:username>', edit_panel_student),
    path('admin/panel/edit/<str:batch>/<str:username>/confirm', edit_panel_student_confirm),
    path('admin/upload/sample_abstract',sample_abstract,name='sample_abstract'),
    path('staff_home/',staff_view,name='staff'),
    path('staff/edit_profile',profile_edit,name='staff_profile_edit'),
    path('staff/change_password',change_password,name='staff_change_password'),
    path('staff/req_under_guidance',under_guidance,name='under_guidance'),
    path('staff/req_under_guidance/<str:batch>', under_guidance_batch),
    path('staff/req_under_guidance/<str:batch>/<str:username>', under_guidance_batch_username),
    path('staff/req_under_guidance/<str:batch>/<str:username>/approve_reject', approve_reject),
    path('staff/req_under_panel',under_panel,name='under_panel'),
    path('staff/req_under_panel/<str:batch>', under_panel_batch),
    path('staff/req_under_panel/<str:batch>/<str:username>', under_guidance_batch_username),
    path('staff/req_under_panel/<str:batch>/<str:username>/approve_reject', approve_reject),
    path('student_home/',student_view,name='student'),
    path('student/complete_profile',complete_profile,name='complete_profile'),
    path('student/edit_profile',profile_edit,name='student_profile_edit'),
    path('student/change_password',change_password,name='student_change_password'),
    path('student/upload_abstract',upload_abstract,name='student_upload_abstract'),
    path('student/upload_report',upload_report,name='student_upload_report'),
    path('student/create_abstract',create_abstract,name='create_abstract'),
    path('student/create_abstract/download',login_required(login_url='login_view')(Pdf.as_view()),name='download_abstract'),
    path('student/approved_copy/download',login_required(login_url='login_view')(Pdf1.as_view()),name='approved_copy'),
    path('student/download_page',download_page,name='download_page'),
    path('student/my_panel',my_panel,name='mypanel'),


    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)