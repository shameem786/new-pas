# Generated by Django 2.0 on 2020-06-01 11:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import phone_field.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Panel_details',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guide', models.IntegerField(blank=True)),
                ('panel1', models.IntegerField(blank=True)),
                ('panel2', models.IntegerField(blank=True)),
                ('panel3', models.IntegerField(blank=True)),
                ('guide_status', models.CharField(blank=True, max_length=20)),
                ('panel1_status', models.CharField(blank=True, max_length=20)),
                ('panel2_status', models.CharField(blank=True, max_length=20)),
                ('panel3_status', models.CharField(blank=True, max_length=20)),
                ('admin_status', models.CharField(blank=True, max_length=20)),
                ('Notes', models.CharField(blank=True, max_length=500)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=100)),
                ('last_name', models.CharField(blank=True, max_length=100)),
                ('email', models.EmailField(max_length=150)),
                ('signup_confirmation', models.BooleanField(default=False)),
                ('admin_approve', models.BooleanField(default=False)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Project_details',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('topic', models.CharField(blank=True, max_length=200)),
                ('domain', models.CharField(blank=True, max_length=100)),
                ('abstract', models.FileField(upload_to='abstract/')),
                ('basepaper', models.FileField(upload_to='basepaper/')),
                ('report', models.FileField(upload_to='report/')),
                ('reject_no', models.IntegerField(blank=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('college_id', models.CharField(max_length=20, unique=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admission_no', models.CharField(max_length=20, unique=True)),
                ('register_no', models.CharField(max_length=20, unique=True)),
                ('mobile_no', phone_field.models.PhoneField(blank=True, help_text='Contact phone number', max_length=31)),
                ('batch', models.IntegerField(max_length=4)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
